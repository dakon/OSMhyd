/*
 * Copyright (C) 2014,2015 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "hydwidget.h"

#include "controlwidget.h"

#include <marble/GeoDataCoordinates.h>
#include <QApplication>
#include <QDateTime>
#include <QGeoCoordinate>
#include <QGeoPositionInfo>
#include <QString>
#include <QStringList>

int
main(int argc, char** argv)
{
	QApplication app(argc,argv);
	Marble::GeoDataCoordinates home;
	double x0, x1, y0, y1;

	switch (app.arguments().count()) {
	case 1:
		home = Marble::GeoDataCoordinates(9.57, 52.27, 0.0, Marble::GeoDataCoordinates::Degree);
		x0 = 9.5;
		y0 = 52.25;
		x1 = 9.6;
		y1 = 52.3;
		break;
	case 2: {
		QStringList l = app.arguments().at(1).split(QLatin1Char(','));
		if (l.count() != 2)
			qFatal("coordinates must be separated by ,");
		x0 = l[0].toDouble();
		y0 = l[1].toDouble();
		home = Marble::GeoDataCoordinates(x0, y0, 0.0, Marble::GeoDataCoordinates::Degree);
		x1 = x0 + 0.03;
		y1 = y0 + 0.05;
		x0 -= 0.03;
		y0 -= 0.05;
		break;
		}
	case 3: {
		QStringList l = app.arguments().at(1).split(QLatin1Char(','));
		QStringList m = app.arguments().at(2).split(QLatin1Char(','));
		if ((l.count() != 2) || (m.count() != 2))
			qFatal("coordinates must be separated by ,");
		x0 = l[0].toDouble();
		y0 = l[1].toDouble();
		x1 = m[0].toDouble();
		y1 = m[1].toDouble();
		home = Marble::GeoDataCoordinates((x1 + x0) / 2, (y1 + y0) / 2, 0.0, Marble::GeoDataCoordinates::Degree);
		break;
	}
	default:
		qFatal("either one or two arguments required");
	}

	// Create a Marble QWidget without a parent
	HydWidget *mapWidget = new HydWidget();

	mapWidget->centerOn(home);
	mapWidget->positionUpdated(QGeoPositionInfo(QGeoCoordinate(home.latitude(Marble::GeoDataCoordinates::Degree), home.longitude(Marble::GeoDataCoordinates::Degree)), QDateTime::currentDateTime()));
	mapWidget->setZoom(2800);

	ControlWidget * const cw = new ControlWidget(mapWidget);
	cw->connect(cw, &ControlWidget::reload, mapWidget, &HydWidget::reload);

	mapWidget->resize(800, 600);
	mapWidget->show();

	return app.exec();
}
