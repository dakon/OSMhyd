project(HydMap C CXX)

cmake_minimum_required(VERSION 3.0)

find_path(MARBLE_INCLUDE_DIR NAMES marble/MarbleModel.h)
find_library(MARBLE_LIBRARIES NAMES marblewidget-qt5 marblewidget)

if (NOT MARBLE_INCLUDE_DIR OR NOT MARBLE_LIBRARIES)
	message(FATAL_ERROR "Marble library not found")
endif ()

find_package(Qt5Core REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5Network REQUIRED)
find_package(Qt5Location REQUIRED)
find_package(Qt5Positioning REQUIRED)

include_directories(${MARBLE_INCLUDE_DIR} ${QT_INCLUDES})

set(CMAKE_AUTOMOC On)
set(CMAKE_AUTORCC On)
set(CMAKE_AUTOUIC On)
set(CMAKE_INCLUDE_CURRENT_DIR On)
set(CMAKE_CXX_FLAGS -std=c++11)

add_executable(hydmap
		main.cpp
		controlwidget.h
		controlwidget.ui
		hydwidget.cpp
		hydwidget.h
		osmhyd.qrc
		osmnode.cpp
		osmnode.h
)

target_link_libraries(hydmap ${MARBLE_LIBRARIES} Qt5::Widgets Qt5::Core Qt5::Network Qt5::Location Qt5::Positioning)
