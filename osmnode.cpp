/*
 * Copyright (C) 2014,2015,2016 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "osmnode.h"

#include <marble/GeoDataCoordinates.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataStyle.h>
#include <marble/GeoDataIconStyle.h>
#include <QImage>
#include <QString>

static const QString key_ref = QLatin1String("ref");
static const QString key_wtank_volume = QLatin1String("water_tank:volume");
static const QString key_diam = QLatin1String("fire_hydrant:diameter");
static const QString key_type = QLatin1String("fire_hydrant:type");

QSharedPointer<Marble::GeoDataStyle> FireHydrant::m_pillarStyle;

OSMNode::OSMNode(double lat, double lon, QHash<QString, QString> attributes)
	: m_pos(lat, lon)
	, m_attributes(attributes)
	, m_placemark(nullptr)
{
}

OSMNode::~OSMNode()
{
	delete m_placemark;
}

Marble::GeoDataPlacemark *
OSMNode::getPlacemark(void)
{
	if (m_placemark == nullptr) {
		m_placemark = new Marble::GeoDataPlacemark(placemarkName());
		m_placemark->setCoordinate(m_pos.longitude(), m_pos.latitude(), 0.0, Marble::GeoDataCoordinates::Degree);
		QSharedPointer<Marble::GeoDataStyle> st = defaultStyle();
		if (!st.isNull())
			m_placemark->setStyle(st);
	}

	return m_placemark;
}

QSharedPointer<Marble::GeoDataStyle>
OSMNode::defaultStyle(void) const
{
	return QSharedPointer<Marble::GeoDataStyle>(nullptr);
}

QString
OSMNode::placemarkName(void) const
{
	return QLatin1String("?");
}

QString
FireHydrant::placemarkName(void) const
{
	QString nm;

	const uint diameter = m_attributes.value(key_diam, "0").toUInt();
	const QString type = m_attributes.value(key_type);

	if (type.isEmpty())
		nm = QLatin1Char('?');
	else if (type == QLatin1String("underground"))
		nm = QLatin1String("UF");
	else
		nm = type;

	if (diameter > 0)
		nm = nm + QLatin1Char(' ') + QString::number(diameter);

	return nm;
}

QSharedPointer<Marble::GeoDataStyle>
FireHydrant::defaultStyle(void) const
{
	if (m_attributes.value(key_type) != QLatin1String("pillar"))
		return OSMNode::defaultStyle();

	if (m_pillarStyle.isNull()) {
		m_pillarStyle.reset(new Marble::GeoDataStyle());
		m_pillarStyle->setIconStyle(Marble::GeoDataIconStyle(QLatin1Literal(":/fire_hydrant.png")));
	}

	return m_pillarStyle;
}


QString
WaterTank::placemarkName(void) const
{
	QString nm = QLatin1String("Z");

	const QString volume = m_attributes.value(key_wtank_volume);
	if (!volume.isEmpty()) {
		uint vol = volume.toUInt();
		if (vol != 0)
			nm += QString::number(vol / 1000);
	}

	return nm;
}

QString
DryRiserInlet::placemarkName(void) const
{
	return QLatin1String("I");
}

QString
EmergencyAccessPoint::placemarkName(void) const
{
	const QString ref = m_attributes.value(key_ref);
	static const QString label_EAP = QLatin1String("EAP");

	if (ref.isEmpty())
		return label_EAP;
	else
		return label_EAP + QLatin1Char(' ') + ref;
}
