/*
 * Copyright (C) 2014,2015,2016 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "hydwidget.h"

#include "osmnode.h"

#include <limits>
#include <marble/GeoDataDocument.h>
#include <marble/GeoDataIconStyle.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataStyle.h>
#include <marble/GeoDataTreeModel.h>
#include <marble/MarbleModel.h>
#include <QBuffer>
#include <QDebug>
#include <QGeoPositionInfo>
#include <QGeoPositionInfoSource>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QNetworkReply>

const QString HydWidget::emg = QLatin1String("emergency");
const QString HydWidget::fh = QLatin1String("fire_hydrant");
const QString HydWidget::wtank = QLatin1String("water_tank");
const QString HydWidget::drinlet = QLatin1String("dry_riser_inlet");
static const QString hw = QLatin1String("highway");
static const QString eap = QLatin1String("emergency_access_point");

using namespace Marble;

HydWidget::HydWidget(QWidget *parent)
	: Marble::MarbleWidget(parent)
	, m_netmgr(new QNetworkAccessManager(this))
	, m_closestStyle(new Marble::GeoDataStyle())
	, m_posMarker(new Marble::GeoDataPlacemark())
	, m_closestMarker(nullptr)
{
	// Load the OpenStreetMap map
	setMapThemeId(QLatin1String("earth/openstreetmap/openstreetmap.dgml"));
	setShowOverviewMap(false);
	setShowCompass(false);
	setShowGrid(false);

	connect(m_netmgr, &QNetworkAccessManager::finished,
			this, &HydWidget::slotDownloadDone);

	QSharedPointer<GeoDataStyle> st(new GeoDataStyle(*m_posMarker->style()));
	st->setIconStyle(GeoDataIconStyle(QLatin1Literal(":/OLmarker.png")));
	m_posMarker->setStyle(st);

	m_closestStyle->setIconStyle(GeoDataIconStyle(QLatin1Literal(":/active.png")));

	QGeoPositionInfoSource * const geosource = QGeoPositionInfoSource::createDefaultSource(this);

	if (geosource) {
		connect(geosource, &QGeoPositionInfoSource::positionUpdated,
			this, &HydWidget::positionUpdated);
		geosource->startUpdates();
	}

	// add the placemark that will be used to show the current position
	Marble::GeoDataDocument * const doc = new Marble::GeoDataDocument();
	doc->append(m_posMarker);

	// Add the document to MarbleWidget's tree model
	model()->treeModel()->addDocument(doc);
}

HydWidget::~HydWidget()
{
	delete m_posMarker;
}

void HydWidget::readData(QIODevice *r)
{
	QJsonDocument jd = QJsonDocument::fromJson(r->readAll());
	GeoDataDocument *document = new GeoDataDocument();
	QJsonArray a = jd.object()["elements"].toArray();

	foreach (const QJsonValue &v, a) {
		if (!v.isObject())
			continue;
		const QJsonObject o = v.toObject();

		static const QString typeToken = QLatin1String("type");
		static const QString nodeString = QLatin1String("node");

		if (o[typeToken].toString() != nodeString)
			continue;

		static const QString idToken = QLatin1String("id");
		static const QString latToken = QLatin1String("lat");
		static const QString lonToken = QLatin1String("lon");
		static const QString tagsToken = QLatin1String("tags");

		QStringList descr;
		double lon = 0.0, lat = 0.0;
		qint64 id;
		bool b = false;

		if (o.contains(idToken)) {
			const QString idStr = o[idToken].toVariant().toString();
			descr << QLatin1String("<a href=\"https://www.openstreetmap.org/node/") + idStr + "\">id: " + idStr + "</a>";

			// I'm a bit paranoid here that this might lose precision for big numbers,
			// so do not take it as a double, but as a string, and then directly cast to int.
			id = idStr.toLongLong(&b);
		}
		if (!b) {
			qDebug() << "invalid or missing id";
			continue;
		}

		if (m_hyds.contains(id))
			continue;

		if (o.contains(latToken)) {
			const QJsonValue l = o[latToken];

			if (l.isDouble())
				lat = l.toDouble();
			else
				continue;
		} else {
			continue;
		}

		if (o.contains(lonToken)) {
			const QJsonValue l = o[lonToken];

			if (l.isDouble())
				lon = l.toDouble();
			else
				continue;
		} else {
			continue;
		}

		QJsonObject tags = o[tagsToken].toObject();
		auto titEnd = tags.constEnd();
		QHash<QString, QString> attributes;

		for (auto it = tags.constBegin(); it != titEnd; it++)
			attributes[it.key()] = it.value().toString();

		// Now all attributes of the returned node are in the hash attributes.
		// Decide if the node is of any interest.
		QScopedPointer<OSMNode> node;

		const auto itEnd = m_filter.constEnd();
		for (auto it = m_filter.constBegin(); (it != itEnd) && node.isNull(); it++) {
			const QString v = attributes.value(it.key());
			if (v.isEmpty())
				continue;

			if (v != it.value())
				continue;

			if (v == fh)
				node.reset(new FireHydrant(lat, lon, attributes));
			else if (v == wtank)
				node.reset(new WaterTank(lat, lon, attributes));
			else if (v == eap)
				node.reset(new EmergencyAccessPoint(lat, lon, attributes));
			else if (v == drinlet)
				node.reset(new DryRiserInlet(lat, lon, attributes));
			else
				node.reset(new OSMNode(lat, lon, attributes));
		}

		if (node.isNull())
			continue;

		GeoDataPlacemark *place = node->getPlacemark();
		place->setDescription(descr.join(QLatin1String("<br/>")));

		m_hyds[id] = node.take();

		document->append( place );
	}

	if (document->featureList().isEmpty()) {
		qDebug() << "data contained no points";
		delete document;
		return;
	}

	// Add the document to MarbleWidget's tree model
	model()->treeModel()->addDocument( document );

	findClosest();
}

void HydWidget::slotDownloadDone(QNetworkReply *r)
{
	r->deleteLater();

	if (r->error() != QNetworkReply::NoError)
		qDebug() << r->errorString();
	else
		readData(r);
}

void HydWidget::loadDataForArea(qreal x0, qreal y0, qreal x1, qreal y1)
{
	/*
	 * (
	 * 	 node["amenity"="fire_hydrant"]  (52.25,9.5,52.3,9.6);
	 * 	 node["emergency"="fire_hydrant"](52.25,9.5,52.3,9.6);
	 * );
	 * out;
	 */

	const QByteArray bbox = QByteArray::number(y0) + "%2C" + QByteArray::number(x0) + "%2C" + QByteArray::number(y1) + "%2C" + QByteArray::number(x1);

	QStringList nodefilter;
	const auto itEnd = m_filter.constEnd();
	for (auto it = m_filter.constBegin(); it != itEnd; it++)
		nodefilter << QString("node%5B%22" + it.key() + "%22%3D%22" + it.value() + "%22%5D(" + bbox + ")%3B");

	const QUrl link = QUrl::fromPercentEncoding("https://overpass-api.de/api/interpreter?data=[out:json]%3B(" + nodefilter.join(QString()).toLatin1() + ")%3Bout%3B");
	qDebug() << link;
	m_netmgr->get(QNetworkRequest(link));
}

void HydWidget::reload(ControlWidget::NodeTypes types)
{
	qreal la0, lo0, la1, lo1;

	m_filter.clear();
	if (types & ControlWidget::FireHydrants)
		m_filter.insert(emg, fh);
	if (types & ControlWidget::WaterTanks)
		m_filter.insert(emg, wtank);
	if (types & ControlWidget::EmergencyAccessPoints)
		m_filter.insert(hw, eap);
	if (types & ControlWidget::DryRiserInlet)
		m_filter.insert(emg, drinlet);

	if (m_filter.isEmpty())
		return;

	geoCoordinates(0, 0, la0, lo0);
	geoCoordinates(width(), height(), la1, lo1);

	const qreal dla = qAbs<qreal>(la1 - la0);
	const qreal dlo = qAbs<qreal>(lo1 - lo0);

	la0 -= (dla / 2);
	la1 += (dla / 2);
	lo0 += (dlo / 2);
	lo1 -= (dlo / 2);

	loadDataForArea(la0, lo1, la1, lo0);
}

void HydWidget::positionUpdated(const QGeoPositionInfo &info)
{
	qDebug() << info;
	if (m_lastPos.coordinate() == info.coordinate()) {
		m_lastPos = info; // to update the timestamp
		return;
	}
	m_lastPos = info;

	const double alt = (m_lastPos.coordinate().type() == QGeoCoordinate::Coordinate3D) ? m_lastPos.coordinate().altitude() : 0.0;
	m_posMarker->setCoordinate(m_lastPos.coordinate().longitude(), m_lastPos.coordinate().latitude(), alt, Marble::GeoDataCoordinates::Degree);

	model()->treeModel()->updateFeature(m_posMarker);
}

void HydWidget::findClosest()
{
	// find the item closest to the current position
	double dist = std::numeric_limits<double>::max();
	OSMNode *c;
	foreach (OSMNode *o, m_hyds) {
		const double d = m_lastPos.coordinate().distanceTo(o->m_pos);
		if (d > dist)
			continue;
		dist = d;
		c = o;
	}

	if (c == m_closestMarker)
		return;

	if (m_closestMarker != nullptr) {
		QSharedPointer<Marble::GeoDataStyle> st = m_closestMarker->defaultStyle();
		if (st.isNull())
			st.reset(new Marble::GeoDataStyle(*c->getPlacemark()->style()));
		m_closestMarker->getPlacemark()->setStyle(st);
	}
	m_closestMarker = c;
	c->getPlacemark()->setStyle(m_closestStyle);

	model()->treeModel()->updateFeature(c->getPlacemark());
}

#include "moc_hydwidget.cpp"
