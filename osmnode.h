/*
 * Copyright (C) 2014,2015,2016 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

#include <qglobal.h>
#include <QGeoCoordinate>
#include <QHash>
#include <QSharedPointer>

namespace Marble {
	class GeoDataPlacemark;
	class GeoDataStyle;
};

class OSMNode {
	OSMNode() = delete;
public:
	OSMNode(double lat, double lon, QHash<QString, QString> attributes = QHash<QString, QString>());
	virtual ~OSMNode();

	const QGeoCoordinate m_pos;
	const QHash<QString, QString> m_attributes;

	Marble::GeoDataPlacemark *getPlacemark(void);

	virtual QSharedPointer<Marble::GeoDataStyle> defaultStyle(void) const;

protected:
	virtual QString placemarkName(void) const;
private:
	Marble::GeoDataPlacemark *m_placemark;
};

class FireHydrant : public OSMNode {
	FireHydrant() = delete;
public:
	FireHydrant(double lat, double lon, QHash<QString, QString> attributes = QHash<QString, QString>())
		: OSMNode(lat, lon, attributes)
	{
	}

	virtual QSharedPointer<Marble::GeoDataStyle> defaultStyle(void) const override;

protected:
	virtual QString placemarkName(void) const;

private:
	static QSharedPointer<Marble::GeoDataStyle> m_pillarStyle;
};

class WaterTank : public OSMNode {
	WaterTank() = delete;
public:
	WaterTank(double lat, double lon, QHash<QString, QString> attributes = QHash<QString, QString>())
		: OSMNode(lat, lon, attributes)
	{
	}

protected:
	virtual QString placemarkName(void) const;
};

class DryRiserInlet : public OSMNode {
	DryRiserInlet() = delete;
public:
	DryRiserInlet(double lat, double lon, QHash<QString, QString> attributes = QHash<QString, QString>())
		: OSMNode(lat, lon, attributes)
	{
	}

protected:
	virtual QString placemarkName(void) const;
};

class EmergencyAccessPoint: public OSMNode {
	EmergencyAccessPoint() = delete;
public:
	EmergencyAccessPoint(double lat, double lon, QHash<QString, QString> attributes = QHash<QString, QString>())
		: OSMNode(lat, lon, attributes)
	{
	}

protected:
	virtual QString placemarkName(void) const;
};
