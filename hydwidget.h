/*
 * Copyright (C) 2014,2015,2016 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

#include "controlwidget.h"

#include <marble/MarbleWidget.h>
#include <QGeoPositionInfo>
#include <QHash>
#include <QSharedPointer>
#include <QString>

class QIODevice;
class QNetworkAccessManager;
class QNetworkReply;

namespace Marble {
	class GeoDataPlacemark;
	class GeoDataStyle;
}

class OSMNode;

class HydWidget: public Marble::MarbleWidget {
	Q_OBJECT
public:
	HydWidget(QWidget *parent = 0);
	virtual ~HydWidget();


public slots:
	void slotDownloadDone(QNetworkReply *r);
	void reload(ControlWidget::NodeTypes types);
	void positionUpdated(const QGeoPositionInfo &info);

private:
	static const QString emg;
	static const QString fh;
	static const QString wtank;
	static const QString drinlet;

	void readData(QIODevice *r);
	void loadDataForArea(qreal x0, qreal y0, qreal x1, qreal y1);

	QHash<qint64, OSMNode *> m_hyds;
	QMultiHash<QString, QString> m_filter;

	QNetworkAccessManager * const m_netmgr;

	const QSharedPointer<Marble::GeoDataStyle> m_closestStyle;
	QGeoPositionInfo m_lastPos;
	Marble::GeoDataPlacemark * const m_posMarker;
	OSMNode *m_closestMarker;

private:
	void findClosest();
};
