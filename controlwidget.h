/*
 * Copyright (C) 2014 Rolf Eike Beer <eike@sf-mail.de>
 */

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#pragma once

#include "ui_controlwidget.h"

class ControlWidget : public QWidget, private Ui::ControlWidget {
	Q_OBJECT
public:
	enum NodeType {
		FireHydrants = 0x1,
		WaterTanks = 0x2,
		EmergencyAccessPoints = 0x4,
		DryRiserInlet = 0x8,
	};
	Q_DECLARE_FLAGS(NodeTypes, NodeType)

	ControlWidget(QWidget *parent = nullptr)
		: QWidget(parent)
	{
		setupUi(this);

		connect(pbLoad, &QPushButton::clicked, this, &ControlWidget::slotReload);
	}

	NodeTypes selectedTypes(void) const
	{
		NodeTypes ret;

		if (cbHydrant->isChecked())
			ret |= FireHydrants;
		if (cbWaterTank->isChecked())
			ret |= WaterTanks;
		if (cbAccessPoint->isChecked())
			ret |= EmergencyAccessPoints;
		if (cbDRInlet->isChecked())
			ret |= DryRiserInlet;

		return ret;
	}

signals:
	void reload(ControlWidget::NodeTypes);

private slots:
	void slotReload()
	{
		emit reload(selectedTypes());
	}
};

Q_DECLARE_OPERATORS_FOR_FLAGS(ControlWidget::NodeTypes)
